var path = require("path");                     // El módulo path lo tenemos por tener instalado NodeJS
var webpack = require("webpack");

module.exports = {                              // Sintaxis CommonJS, sólo la usaremos aquí
    entry: ["bootstrap-loader", "./src/js/index.js"],               // Para que funcione el loader de BS, debe estar aquí. El segundo es nuestro script de entrada
    output: {
        filename: "bundle.js",                  // Nombre del bundle de salida
        path: path.resolve(__dirname, "dist"),  // Ruta física absoluta donde se guardará el bundle
        publicPath: "/dist/"                    // URL pública para acceder al bundle
    },
    devServer: { 
        contentBase: path.join(__dirname, "public"),
        open: "Chrome",                         // Para que lo abra desde el Chrome, aunque no sea el navegador por defecto
        hot: true
    },
    devtool: "inline-source-map",               // Generará source maps dentro de los mismos archivos JS
    module: {
        rules: [
            { 
                test: /bootstrap-sass(\\|\/)assets(\\|\/)javascripts(\\|\/)/,       // Para la parte JS de Bootstrap
                loader: 'imports-loader?jQuery=jquery' 
            },  
            {
                test: /\.scss$/,                                    // Para los archivos con extensión .scss
                use: ["style-loader", "css-loader", "sass-loader"]  // utiliza primero sass-loader, luego css-loader y luego style-loader
            },
            {
                test: /\.js$/,                  // Para los archivos con extensión .js
                exclude: /node_modules/,        // excepto los que estén en la carpeta node_modules
                use: {
                    loader: "babel-loader",     // usa el cargador de Babel
                    query: {
                        presets: ["env"]        // con el preset env
                    }
                }
            },
            { 
                test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,     // Para las tipografías que se cargan desde CSS (p.e, Glyphicons)
                loader: 'url-loader'
            },   
            {
                test: /\.html$/,                // Para los archivos con extensión .html
                use: ["raw-loader"]             // usa el cargador raw-loader  (para que webpack-dev-server refresque cuando cambiemos el HTML)
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
}